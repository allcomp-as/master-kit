#!/bin/sh
# Install script for MasterKit RasPI image

# Check if run under root
if [ "`whoami`" != 'root' ]; then
  echo "Please, run as root!"
  exit 1;
fi

echo 'Root-check OK'


### Basic configuration
### ===================
echo
echo 'Basic configuration'

# Hostname
HOSTNAME='master-kit'

echo "Changing hostname to '$HOSTNAME' ..."
hostname "$HOSTNAME"
echo "$HOSTNAME" > /etc/hostname
echo ' DONE'

# Enable SSH server
echo "Enabling SSH server ..."
systemctl enable ssh.service
echo ' DONE'

# Block requests to SHS from outside
echo 'Configuring firewall for SHS ...'

iptables -t filter -A INPUT \
		-j ACCEPT \
	--in-interface lo

iptables -t filter -A INPUT \
		-j REJECT --reject-with icmp-port-unreachable \
	-p tcp \
	--dport 81

ip6tables -t filter -A INPUT \
		-j ACCEPT \
	--in-interface lo

ip6tables -t filter -A INPUT \
		-j REJECT --reject-with icmp6-port-unreachable \
	-p tcp \
	--dport 81

echo ' DONE'


### Basic software
### ==============
echo
echo 'Installing basic software (BashCompletion, HTop, Midnight Commander) ...'
apt-get install -y \
	bash-completion \
	htop \
	mc
echo ' DONE'


### Change default password
### =======================
PASS='allcomp.cz'

echo
echo "Changing default password for user pi to '$PASS' ..."
echo "pi:$PASS" | chpasswd
echo ' DONE'


### Wireless hotspot & routing
### ==========================
echo
echo 'Setting-up hotspot & routing'

# Install SW
echo 'Installing required software ...'
apt-get install -y \
	dnsmasq \
	hostapd
echo ' DONE'

# Configure hostapd
echo 'Configuring hostapd ...'

cat > /etc/hostapd/hostapd.conf <<- END
	interface=wlan0
	driver=nl80211
	ssid=MasterKit
	hw_mode=g
	channel=7
	wmm_enabled=0
	macaddr_acl=0
	auth_algs=1
	ignore_broadcast_ssid=0
	wpa=2
	wpa_passphrase=$PASS
	wpa_key_mgmt=WPA-PSK
	wpa_pairwise=TKIP
	rsn_pairwise=CCMP
END

echo >> /etc/default/hostapd
echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> /etc/default/hostapd

echo ' DONE'

# Configure dnsmasq
echo 'Configuring dnsmasq ...'

cat > /etc/dnsmasq.hosts <<- 'END'
	192.168.124.1     master-kit
	192.168.124.1     island
END

echo >> /etc/default/dnsmasq
echo 'DNSMASQ_OPTS="-h -H /etc/dnsmasq.hosts"' >> /etc/default/dnsmasq

cat > /etc/dnsmasq.conf << 'END'
interface=wlan0
	dhcp-range=192.168.124.100,192.168.124.200,255.255.255.0,24h
END

echo >> /etc/dhcpcd.conf
echo >> /etc/dhcpcd.conf
cat >> /etc/dhcpcd.conf << 'END'
interface wlan0
	static ip_address=192.168.124.1/24
	nohook wpa_supplicant
END

echo ' DONE'

# Enable routing
echo 'Enabling routing ...'
echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/ipv4_forward.conf
echo ' DONE'

# Reload systemd
echo 'Daemon-reloading systemd ...'
systemctl daemon-reload
echo ' DONE'

# Un-mask & enable hostapd service
echo 'Unmasking & enabling hostapd service ...'
systemctl unmask hostapd.service
systemctl enable hostapd.service
echo ' DONE'

# NAT
echo 'Setting-up NAT ...'
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
echo ' DONE'

# Make firewall rules persistent (e.g. auto-load current rules during boot)
echo 'Persistent firewall rules ...'

debconf-set-selections <<- 'END'
	iptables-persistent	iptables-persistent/autosave_v4 \
		boolean true
	iptables-persistent	iptables-persistent/autosave_v6 \
		boolean true
END

apt-get install -y \
	iptables-persistent
echo ' DONE'


### Finish
### ======

# Enable Chromium auto-start when run in desktop image
if dpkg-query -s lxde &> /dev/null; then
	mkdir -p /home/pi/.config/lxsession/LXDE-pi
	cp /etc/xdg/lxsession/LXDE-pi/autostart /home/pi/.config/lxsession/LXDE-pi/autostart
	echo '@chromium-browser http://localhost/welcome' >> /home/pi/.config/lxsession/LXDE-pi/autostart
fi

# End
echo 'Basic installation finished successfully.'
