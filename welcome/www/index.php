<?php


/**
 * Parse Accept-Language header and returns preferred language.
 * Based on method from Nette\Http\Request (Nette Framework - nette.org)
 * @param  string[] supported languages
 * @param  string|null default language if no was matched
 * @return string|null
 */
function detectLanguage(array $langs, $default = null){
	if(!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
		return $default;
	};
	
	$header = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	
	$s = strtolower($header);// case insensitive
	$s = strtr($s, '_', '-');// cs_CZ means cs-CZ
	rsort($langs);           // first more specific
	preg_match_all('#(' . implode('|', $langs) . ')(?:-[^\s,;=]+)?\s*(?:;\s*q=([0-9.]+))?#', $s, $matches);

	if(!$matches[0]){
		return $default;
	};
	
	$max = 0;
	$lang = null;
	foreach($matches[1] as $key => $value){
		$q = $matches[2][$key] === '' ? 1.0 : (float) $matches[2][$key];
		if($q > $max){
			$max = $q;
			$lang = $value;
		};
	};
	
	return ($lang !== null) ? $lang : $default;
} // detectLanguage


//-------------------- BEGIN MAIN --------------------
$langs = ['en', 'cs'];
$lang = null;

if(isset($_SERVER['REQUEST_URI'])){
	$path = strval($_SERVER['REQUEST_URI']);
	if(strlen($path) >= 11){
		$path = substr($path, 9);
		
		if(in_array($path, $langs, true)){
			$lang = $path;
		};
	};
};

if($lang === null){
	$lang = detectLanguage($langs, $lang[0]);
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/welcome/' . $lang);
	// no exit here - if there will be a problem with redirecting, the correct page will be always displayed
};


//-------------------- BEGIN HTML --------------------
?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8">
	<title><?php if($lang == 'cs'){?>Vítejte<?php } else { ?>Welcome<?php }; ?> :: MasterKit</title>
	<meta name="Language" content="<?php echo $lang; ?>">
	<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="large-welcome">
		<?php if($lang == 'cs'){?>
			Děkujeme za zakoupení<br />
			stavebnice MasterKit!
		<?php } else { ?>
			Thank You for purchasing our<br />
			MasterKit!
		<?php }; ?>
	</div>
	<div class="content">
		<?php if($lang == 'cs'){?>
			<h2>Stavebnice</h2>
			<p>
				Hlavní součásti stavebnice jsou:
				<ul>
					<li>Raspberry PI 3B+</li>
					<li>16GB microSD karta</li>
					<li>Modul Island 1010</li>
					<li>CAN převodník</li>
					<li>LED DPS pro otestování funkce výstupů</li>
				</ul>
			</p>
			
			<h2>Island 1010</h2>
			<p>
				Modul Island 1010 komunikuje s PC přes fyzickou vrstvu sběrnice CAN.
				Sběrnice CAN je široce používaná v průmyslu, zejména pak v automobilovém.
				Tato sběrnice je velmi odolná proti elektromagnetickému rušení a zajišťuje spolehlivou komunikaci mezi propojenými zařízeními.
			</p>
			<p>
				Modul Island 1010 poskytuje 10 digitálních vstupů a 10 digitálních výstupů (5 výstupů navíc může být použito jako PWM výstupy).
			</p>
			<p>
				Vstupy i výstupy jsou číslované zleva od 1 do 10 (při pohledu shora na modul se vstupními/výstupními konektory směřujícími nahoru).
				Na vrchní straně modulu je navíc 10 přepínačů, které umožňují ovládat výstupy nezávisle na zbylé elektronice.
				U spínačů se nachází dvoubarevné LED signalizující sepnutí příslušného výstupu
				(zelená = výstup sepnut elektronikou, červená = výstup sepnut spínačem, zhasnuto = výstup vypnutý).
				Výstupní svorkovnice je rovněž součástí horní DPS, zatímco vstupní svorkovnice je na společném DPS s řídicí elektronikou.
			</p>
			<p>
				Sepnutí vstupů se provede spojením příslušné svorky se zemí - svorkou GND (naznačeno ve schématu zapojení na krabici).
			</p>
			
			<h2>Aplikace Revolution Web</h2>
			<p>
				Do ovládací aplikace se dostanete odkazem: <a href="../" target="_blank">Revolution Web App</a>.
				Zde jsme pro Vás již plně nastavili modul Island 1010 z Vaší stavebnice, zbývá jen vyzkoušet.
			</p>
			<p>
				Tato aplikace tak nyní nabízí 10 přepínačů pro ovládání jednotlivých výstupů a 10 indikátorů pro zobrazení stavu vstupů.
				Rovněž jsou zde již přednastaveny závislosti mezi vstupy a výstupy.
				Vše lze upravit v administraci: <a href="../admin/" target="_blank">Revolution Web App - Administration</a>.
				Pro přístup do nastavení je vyžadováno přihlášení - výchozím uživatelem je 'admin' s heslem '0000'.
			</p>
			
			<h2>Další informace</h2>
			<p>
				Všechny informace o stavebnici MasterKit a modulu Island 1010, stejně jako obraz na SD kartu, aktualizace a další novinky naleznete na stránkách:
				<a href="https://allcomp.cz/island1010" target="_blank">https://allcomp.cz/island1010</a>.
			</p>
			<p>
				Rovněž můžete navštívit stránky naší společnosti: <a href="https://allcomp.cz/">https://allcomp.cz/</a>.
			</p>
			<p>
				A náš blog zde: <a href="https://allcomp.cz/blog" target="_blank">https://allcomp.cz/blog</a>.
			</p>
		<?php } else { ?>
			<h2>Quick Info</h2>
			<p>
				Main parts of the kit:
				<ul>
					<li>Raspberry PI 3B+</li>
					<li>16GB microSD card</li>
					<li>Island 1010 module</li>
					<li>CAN converter</li>
					<li>LED PCB for testing of outputs</li>
				</ul>
			</p>
			
			<h2>Island 1010</h2>
			<p>
				The Island 1010 module communicates with PC through physical layer of CAN bus.
				CAN bus is widely used in industry, mainly in the automotive.
				This bus is highly resistant to electromagnetic interference and provides reliable communication between devices.
			</p>
			<p>
				Island 1010 provides 10 digital inputs and 10 digital outputs (5 outputs can be also used as PWM outputs).
			</p>
			<p>
				Inputs and outputs are numbered from left to right from 1 to 10 (when looking at the module's front side with input/output connectors facing up).
				The front face of module consists of 10 switches, which control outputs independently of the electronics.
				There are also dual-color LEDs nearby the switches, that signalize the state of each output
				(green = output on via electronics, red = output forced on by switch, LED off = output off).
				The output sockets are also located at the top PCB, while the input ones are at the bottom PCB together with module's main electronics.
			</p>
			<p>
				The inputs can be activated by connecting the input sockets to the ground - the GND socket (see wiring diagram on the paper box)
			</p>
			
			<h2>Revolution Web Application</h2>
			<p>
				For You, we have put into operation the whole Island 1010 module from the kit.
				All You need to do is visiting this link: <a href="../" target="_blank">Revolution Web App</a> and test it's functionality.
			</p>
			<p>
				This web application provides 10 switches for outputs and 10 indicators of inputs.
				For each input and output pair there is some programmed operating logic.
				You can change this logic in administration: <a href="../admin/" target="_blank">Revolution Web App - Administration</a>.
				This application requires login - You can use the default user 'admin' with password '0000' to access it.
			</p>
			<p>Try to play with it around, and have fun!</p>
			
			<h2>Other links</h2>
			<p>
				All information about the MasterKit and Island 1010 module together with SD card image, updates and news can be found here:
				<a href="https://allcomp.cz/island1010" target="_blank">https://allcomp.cz/island1010</a>.
				(Note: You can switch the language via icon at the top of the pages.)
			</p>
			<p>
				If You would like to visit our company's website, it can be found here: <a href="https://allcomp.cz/">https://allcomp.cz/</a>.
			</p>
			<p>
				And our blog here: <a href="https://allcomp.cz/blog" target="_blank">https://allcomp.cz/blog</a>.
			</p>
			<p>We are doing our best to translate all content into English.</p>
		<?php }; ?>
	</div>
</body>
</html>
