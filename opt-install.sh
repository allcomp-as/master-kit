#!/bin/sh
# Install script for MasterKit software

# Assumes that SHS is already present in the ./shs/ directory.
# If this is not the case, download it from https://www.allcomp.cz
# or copy it from other system first.


# Check if run under root
if [ "`whoami`" != 'root' ]; then
  echo "Please, run as root!"
  exit 1;
fi

echo 'Root-check OK'



### Install SW
### ==========
echo
echo 'Install SW'

echo 'Installing required software (LAMP stack) ...'
apt-get install -y \
	apache2 \
	mariadb-server \
	php \
	php-mbstring \
	php-mysql
echo ' DONE'

echo 'Installing software required for set-up (MySQL client, Composer) ...'
apt-get install -y \
	composer \
	mariadb-client
echo ' DONE'

echo "Disabling Apache's default VirtualHost ..."
a2dissite 000-default
systemctl reload apache2
echo ' DONE'

echo 'Installing required software (Java runtime & library) ...'
apt-get install -y \
	default-jre \
	libgcj15
echo ' DONE'



### Adminer
### =======
echo
echo 'Setting-up Adminer'

# Install
echo 'Installing ...'
apt-get install -y \
	adminer
echo ' DONE'

# Configure Apache
echo 'Configuring Apache ...'

cat > /etc/apache2/conf-available/adminer.conf << 'END'
Alias /adminer/static /usr/share/adminer/adminer/static
Alias /adminer /usr/share/adminer/adminer/index.php

<Directory /usr/share/adminer/adminer>
	Require all denied
	
	<Files index.php>
		Require all granted
	</Files>
</Directory>

<Directory /usr/share/adminer/adminer/static>
	Require all granted
</Directory>
END

echo ' DONE'

echo "Enabling Adminer in Apache ..."
a2enconf adminer
systemctl reload apache2
echo ' DONE'



### SHS
### ===
echo
echo 'Setting-up SHS ...'


# Copy to target location
echo 'SHS - Copying into /opt ...'
mkdir -p /opt/allcomp/revolution/shs
cp -R ./shs/* /opt/allcomp/revolution/shs/
echo ' DONE'


# Database
echo 'SHS - Preparing DB - loading schema ...'
mysql < /opt/allcomp/revolution/shs/DB.sql
echo ' DONE'

echo 'SHS - Preparing DB - creating user ...'
mysql < /opt/allcomp/revolution/shs/DB-user.sql
echo ' DONE'

echo 'SHS - Preparing DB - loading initial (demo) data ...'
mysql < demo-data_shs.sql
echo ' DONE'


# Set-up systemd services
echo 'SHS - Setting-up systemd services'

echo 'Symlinking services ...'
ln -s \
	/opt/allcomp/revolution/shs/allnet.service \
	/etc/systemd/system/allnet.service
ln -s \
	/opt/allcomp/revolution/shs/shs.service \
	/etc/systemd/system/shs.service
echo ' DONE'


# Reload systemd
echo 'Daemon-reloading systemd ...'
systemctl daemon-reload
echo ' DONE'


# Enable services
echo 'Enabling services ...'
systemctl enable shs.service
echo ' DONE'



### Web
### ===
echo
echo 'Setting-up Web ...'


# Retrieve from GitLab & unpack into target location
echo 'Web - downloading ...'
wget --no-clobber https://gitlab.com/allcomp-as/revolution/web/-/archive/ee141923a14d478e34d496cde6da962e9dd95239/web-ee141923a14d478e34d496cde6da962e9dd95239.tar.bz2
echo ' DONE'

echo 'Unpacking & copying into /opt ...'
mkdir -p /opt/allcomp/revolution/web
tar \
	--extract \
	--bzip2 \
	--verbose \
	--file web-ee141923a14d478e34d496cde6da962e9dd95239.tar.bz2
cp -R web-ee141923a14d478e34d496cde6da962e9dd95239/* /opt/allcomp/revolution/web/
rm -R web-ee141923a14d478e34d496cde6da962e9dd95239/
echo ' DONE'

echo 'Installing dependencies ...'
chmod 777 /opt/allcomp/revolution/web/vendor/
su -c 'cd /opt/allcomp/revolution/web/ && composer install' - pi
chmod 755 /opt/allcomp/revolution/web/vendor/
chown -R root:root /opt/allcomp/revolution/web/vendor/
echo ' DONE'


# Database
echo 'Web - Preparing DB - loading schema ...'
mysql < /opt/allcomp/revolution/web/DB.sql
echo ' DONE'

echo 'Web - Preparing DB - creating user ...'
mysql < /opt/allcomp/revolution/web/DB-user.sql
echo ' DONE'

echo 'Web - Preparing DB - loading initial (demo) data ...'
mysql < demo-data_web.sql
echo ' DONE'


# Set-up WebServer
echo "Setting-up Apache's VirtualHost ..."
ln -s \
	/opt/allcomp/revolution/web/shs.conf \
	/etc/apache2/sites-enabled/shs.conf
systemctl reload apache2
echo ' DONE'



### Welcome page
### ============
echo
echo 'Setting-up Welcome page ...'


# Copy to target location
echo 'Welcome - Copying into /opt ...'
mkdir -p /opt/allcomp/master-kit/welcome
cp -R ./welcome/* /opt/allcomp/master-kit/welcome/
echo ' DONE'


# Set-up WebServer
echo 'Welcome - Setting-up Apache ...'
ln -s \
	/opt/allcomp/master-kit/welcome/welcome.conf \
	/etc/apache2/conf-enabled/welcome.conf
a2enmod rewrite
systemctl reload apache2
echo ' DONE'
