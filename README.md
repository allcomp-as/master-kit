MasterKit
=========
Allcomp MasterKit scripts...


SD card preparation
-------------------
1) Update & upgrade
```bash
apt-get update
apt-get upgrade
```

2) Configure RasPI
```bash
raspi-config
```
Disable login shell on serial & enable it as standard interface.

3) Reboot
	IMPORTANT - Reboot after the upgrade, otherwise the firewall rules won't be saved during the installation.

4) Install basic SW
```bash
./install.sh
```

5) Install additional SW (revolution server, ...)
```bash
./opt-install.sh
```

6) Reboot
	The newly installed services will start after reboot (they are not started immediatelly during installation - clean start is better).
