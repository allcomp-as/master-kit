SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `revolution_smarthouse`;

INSERT INTO `control_units` (`id`, `software_address`, `hardware_address`, `value_type`, `security`, `description`, `active_state`, `simulate`, `last_state`, `timer`, `timer_target_state`, `activation_delay`, `ewc_version`, `metadata`) VALUES
(1,	1,	'EWC01_OUT0',	0,	'',	'Module1 - Output 1',	1,	1,	0,	1530616550689,	0,	0,	2,	''),
(2,	2,	'EWC01_OUT1',	0,	'',	'Module1 - Output 2',	1,	1,	1,	0,	0,	0,	2,	''),
(3,	3,	'EWC01_OUT2',	0,	'',	'Module1 - Output 3',	1,	1,	0,	0,	0,	0,	2,	''),
(4,	4,	'EWC01_OUT3',	0,	'',	'Module1 - Output 4',	1,	1,	1,	0,	0,	0,	2,	''),
(5,	5,	'EWC01_OUT4',	0,	'',	'Module1 - Output 5',	1,	1,	1,	0,	0,	0,	2,	''),
(6,	6,	'EWC01_OUT5',	0,	'',	'Module1 - Output 6',	1,	1,	0,	0,	0,	0,	2,	''),
(7,	7,	'EWC01_OUT6',	0,	'',	'Module1 - Output 7',	1,	1,	0,	1530616552334,	0,	0,	2,	''),
(8,	8,	'EWC01_OUT7',	0,	'',	'Module1 - Output 8',	1,	1,	0,	1530616552539,	0,	0,	2,	''),
(9,	9,	'EWC01_OUT8',	0,	'',	'Module1 - Output 9',	1,	1,	0,	1530616552777,	0,	0,	2,	''),
(10,	10,	'EWC01_OUT9',	0,	'',	'Module1 - Output 10',	1,	1,	0,	1530616552973,	0,	0,	2,	''),
(11,	11,	'EWC01_IN16',	0,	'',	'Module1 - Input 1',	1,	0,	0,	0,	0,	0,	2,	''),
(12,	12,	'EWC01_IN17',	0,	'',	'Module1 - Input 2',	0,	0,	0,	0,	0,	0,	2,	''),
(13,	13,	'EWC01_IN18',	0,	'',	'Module1 - Input 3',	0,	0,	0,	0,	0,	0,	2,	''),
(14,	14,	'EWC01_IN19',	0,	'',	'Module1 - Input 4',	0,	0,	0,	0,	0,	0,	2,	''),
(15,	15,	'EWC01_IN20',	0,	'',	'Module1 - Input 5',	1,	1,	0,	0,	0,	0,	2,	''),
(16,	16,	'EWC01_IN21',	0,	'',	'Module1 - Input 6',	1,	1,	0,	0,	0,	0,	2,	''),
(17,	17,	'EWC01_IN22',	0,	'',	'Module1 - Input 7',	1,	1,	0,	0,	0,	0,	2,	''),
(18,	18,	'EWC01_IN23',	0,	'',	'Module1 - Input 8',	1,	1,	0,	0,	0,	0,	2,	''),
(19,	19,	'EWC01_IN25',	0,	'',	'Module1 - Input 9',	1,	1,	0,	0,	0,	0,	2,	''),
(20,	20,	'EWC01_IN24',	0,	'',	'Module1 - Input 10',	1,	1,	0,	0,	0,	0,	2,	'');

INSERT INTO `output_states` (`id`, `software_id`, `time`, `value`) VALUES
(1,	2,	1528109746203,	0),
(2,	3,	1528109837322,	1),
(3,	8,	1552399031631,	0);


INSERT INTO `security_events` (`id`, `control_unit`, `type`, `metadata`, `conditions`, `security_id`) VALUES
(8,	97,	0,	'duration=25000',	'',	1),
(9,	1,	2,	'phoneNumber=+420777323137;message=[VejminekSnu - ALARM] %firstTriggerDescription%',	'',	1);

INSERT INTO `security_sectors` (`id`, `name`, `notification_tolerance`, `activation_delay`, `last_state`) VALUES
(1,	'Celý dům',	1,	0,	0),
(2,	'Strojovna',	1,	0,	0);


INSERT INTO `trigger_events` (`id`, `trigger`, `target`, `type`, `metadata`, `group_id`, `activation_conditions`, `deactivation_conditions`) VALUES
(2,	11,	1,	1,	'',	1,	'',	''),
(3,	12,	2,	1,	'',	2,	'',	''),
(4,	13,	3,	1,	'reversed=true',	3,	'',	''),
(5,	14,	4,	1,	'reversed=true',	4,	'',	''),
(6,	15,	5,	0,	'',	5,	'',	''),
(7,	16,	6,	0,	'',	6,	'',	''),
(8,	17,	7,	0,	'',	7,	'',	''),
(9,	18,	8,	0,	'',	8,	'',	''),
(10,	19,	9,	0,	'',	9,	'',	''),
(11,	20,	10,	0,	'',	10,	'',	'');
