SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `revolution_smart_control`;

INSERT INTO `controls` (`id`, `room`, `name`, `outputs`, `type`, `icon`, `rank`, `last_time_usage`) VALUES
(12,	1,	'Output 1',	'1',	0,	'bulb.png',	0,	1552374825544),
(13,	1,	'Output 2',	'2',	0,	'bulb.png',	0,	1552376323923),
(14,	1,	'Output 3',	'3',	0,	'bulb.png',	0,	1551959246954),
(15,	1,	'Output 4',	'4',	0,	'bulb.png',	0,	1552376324384),
(16,	1,	'Output 5',	'5',	0,	'bulb.png',	0,	1552376323352),
(17,	1,	'Output 6',	'6',	0,	'bulb.png',	0,	1552376325437),
(18,	1,	'Output 7',	'7',	0,	'bulb.png',	0,	1552376325779),
(19,	1,	'Output 8',	'8',	0,	'bulb.png',	0,	1552376327411),
(20,	1,	'Output 9',	'9',	0,	'bulb.png',	0,	1551959249254),
(21,	1,	'Output 10',	'10',	0,	'bulb.png',	0,	1551959036772),
(22,	1,	'Input 1',	'11',	6,	'default.png',	0,	0),
(23,	1,	'Input 2',	'12',	6,	'default.png',	0,	0),
(24,	1,	'Input 3',	'13',	6,	'default.png',	0,	0),
(25,	1,	'Input 4',	'14',	6,	'default.png',	0,	0),
(26,	1,	'Input 5',	'15',	6,	'default.png',	0,	0),
(27,	1,	'Input 6',	'16',	6,	'default.png',	0,	0),
(28,	1,	'Input 7',	'17',	6,	'default.png',	0,	0),
(29,	1,	'Input 8',	'18',	6,	'default.png',	0,	0),
(30,	1,	'Input 9',	'19',	6,	'default.png',	0,	0),
(31,	1,	'Input 10',	'20',	6,	'default.png',	0,	0);

INSERT INTO `macros` (`id`, `name`, `command`) VALUES
(1,	'Turn Everything ON',	'type=value,cu=1,delay=0,value=1;type=value,cu=2,delay=0,value=1;type=value,cu=3,delay=0,value=1;type=value,cu=4,delay=0,value=1;type=value,cu=5,delay=0,value=1;type=value,cu=6,delay=0,value=1;type=value,cu=7,delay=0,value=1;type=value,cu=8,delay=0,value=1;type=value,cu=9,delay=0,value=1;type=value,cu=10,delay=0,value=1'),
(2,	'Turn Everything OFF',	'type=value,cu=1,delay=0,value=0;type=value,cu=2,delay=0,value=0;type=value,cu=3,delay=0,value=0;type=value,cu=4,delay=0,value=0;type=value,cu=5,delay=0,value=0;type=value,cu=6,delay=0,value=0;type=value,cu=7,delay=0,value=0;type=value,cu=8,delay=0,value=0;type=value,cu=9,delay=0,value=0;type=value,cu=10,delay=0,value=0');


INSERT INTO `rooms` (`id`, `name`, `floor`) VALUES
(1,	'Module 1',	0);

INSERT INTO `security_systems` (`id`, `name`) VALUES
(1,	'Celý dům'),
(2,	'Strojovna');

INSERT INTO `users` (`id`, `username`, `password`, `permission_level`, `last_ip`) VALUES
(1,	'admin',	'4a7d1ed414474e4033ac29ccb8653d9b',	99,	'192.168.124.1');
